# EJERCICIO 16.8. CALCULADORA

# Suma
def suma(a, b):
    return a + b


# Resta
def resta(a, b):
    return a - b


if __name__ == "__main__":
    # Suma de 1 y 2
    print(f"La suma es {suma(1, 2)}.")
    # Suma de 3 y 4
    print(f"La suma es {suma(3, 4)}.")
    # Resta de 5 y 6
    print(f"La resta es {resta(5, 6)}.")
    # Resta de 7 y 8
    print(f"La resta es {resta(7, 8)}.")


